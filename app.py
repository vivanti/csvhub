import os, string, random, re
import filetype
import streamlit as st
import pandas as pd
import numpy as np
import snowflake.connector
from google.cloud import storage

# configure CSVHub
APP_TITLE     = 'CSVHub'
APP_QUERY_TAG = 'csvhub'

# configure Snowflake
SNOWFLAKE_DATABASE    = os.environ.get('CSVHUB_SNOWFLAKE_DATABASE',    'csvhub')
SNOWFLAKE_SCHEMA      = os.environ.get('CSVHUB_SNOWFLAKE_SCHEMA',      'uploads')
SNOWFLAKE_META_SCHEMA = os.environ.get('CSVHUB_SNOWFLAKE_META_SCHEMA', 'public')
SNOWFLAKE_FILE_FORMAT = os.environ.get('CSVHUB_SNOWFLAKE_FILE_FORMAT', 'csv')

DEFAULT_SNOWFLAKE_ACCOUNT   = os.environ.get('CSVHUB_DEFAULT_SNOWFLAKE_ACCOUNT', '')
DEFAULT_SNOWFLAKE_ROLE      = os.environ.get('CSVHUB_DEFAULT_SNOWFLAKE_ROLE', '')
DEFAULT_SNOWFLAKE_WAREHOUSE = os.environ.get('CSVHUB_DEFAULT_SNOWFLAKE_WAREHOUSE', '')

ALLOWED_SNOWFLAKE_ACCOUNTS = os.environ.get('CSVHUB_ALLOWED_SNOWFLAKE_ACCOUNTS', DEFAULT_SNOWFLAKE_ACCOUNT).split(',')

# configure Google Cloud Storage
GOOGLE_PROJECT     = os.environ.get('CSVHUB_GOOGLE_PROJECT')
GOOGLE_BUCKET      = os.environ.get('CSVHUB_GOOGLE_BUCKET')
GOOGLE_BLOB_PREFIX = 'csvhub/' + os.environ.get('CSVHUB_GOOGLE_BLOB_PREFIX', '')

SERVICE_ACCOUNT_JSON_FILE = '/creds/google.json'

# configure Streamlit
st.set_page_config(
    page_title = f'{APP_TITLE} - made with ❤️ by Vivanti',
    menu_items = None,
)
st.image('https://images.squarespace-cdn.com/content/v1/63e5084f4bfd5337dee37878/87e8fa1f-a064-4b97-a1eb-7b40143ed063/vivanti-logo.png?format=100w')
st.title(APP_TITLE)

def clean_name(s):
  return re.sub(r'_$', '', re.sub(r'^([0-9])', '_\\1', re.sub(r'[^a-zA-Z0-9]+', '_', s)))

# set up session state with defaults
if 'snowflake' not in st.session_state:
  st.session_state['snowflake'] = None

# aaaaaaand GO!
with st.sidebar:
  st.title('Connect to Snowflake')
  account = st.text_input('Account', placeholder = 'i.e. abc1234567.us-central1.gcp', value = DEFAULT_SNOWFLAKE_ACCOUNT)
  username = st.text_input('Username')
  password = st.text_input('Password', type = 'password')
  role = st.text_input('Role', value = DEFAULT_SNOWFLAKE_ROLE)
  warehouse = st.text_input('Warehouse', value = DEFAULT_SNOWFLAKE_WAREHOUSE)
  login = st.button('Login', type='primary')

  if login:
    if len(ALLOWED_SNOWFLAKE_ACCOUNTS) > 0 and account not in ALLOWED_SNOWFLAKE_ACCOUNTS:
      st.write('That Snowflake account is not allowed to use this instance of CSVHub.')
      st.session_state['snowflake'] = None
    else:
      st.session_state['snowflake'] = snowflake.connector.connect(
        user = username,
        password = password,
        account = account,
        role = role,
        warehouse = warehouse,
        session_parameters = {
          'QUERY_TAG': APP_QUERY_TAG
        }
      )

if st.session_state['snowflake'] is None:
  st.write("Please log into Snowflake with your Vivanti credentials")

else:
  data_source = 'CSV'
  c = st.session_state['snowflake'].cursor()

  file = st.file_uploader("Upload a CSV file (or an Excel spreadsheet)")
  if file:
    ext = filetype.guess_extension(file)
    df = None

    st.write(f"## {file.name}")
    if ext == 'xlsx':
      st.write('It looks like you uploaded an Excel spreadsheet...')
      data_source = 'Excel'
      edf = pd.ExcelFile(file)
      chosen = st.selectbox('What sheet would you like to import?', edf.sheet_names)
      if chosen:
        df = pd.read_excel(file, chosen)

    else:
      df = pd.read_csv(file)

    if df is not None:
      column_renames = {}
      for u_col in df.columns:
        col = clean_name(u_col)
        if col != u_col:
          column_renames[u_col] = col

      df = df.rename(columns=column_renames)
      st.write(df)

      u_table = st.text_input(f"What table should I load this {data_source} into (in CSVHUB.UPLOADS)?")
      if u_table:
        table = clean_name(u_table)
        st.write(f"## Loading {data_source} data &rarr; {table}")
        if table != u_table:
          st.write(f'(I cleaned up the table name from what you entered: "{u_table}")')
        st.write(f"Please stand by while we stage and load {file.name} into {SNOWFLAKE_SCHEMA}.{table}...")

        destination_blob_name = GOOGLE_BLOB_PREFIX + "".join(random.choices(string.ascii_lowercase, k=19)) + '.csv'
        st.write(f"... uploading {data_source} file &rarr; gcs://{GOOGLE_BUCKET}/{destination_blob_name}")

        storage_client = storage.Client.from_service_account_json(SERVICE_ACCOUNT_JSON_FILE)
        bucket = storage_client.bucket(GOOGLE_BUCKET)
        blob = bucket.blob(destination_blob_name)
        blob.upload_from_string(df.to_csv(index=False))

        st.write(f"... loading data into {SNOWFLAKE_SCHEMA}.{table} table")
        sql = f'''
select count(*)
  from {SNOWFLAKE_DATABASE}.information_schema.tables
 where table_schema = upper('{SNOWFLAKE_SCHEMA}')
   and table_name = upper(%s)
'''
        st.write(f'```{sql}```'.replace('%s', f"'{table}'"))
        (n,) = c.execute(sql, (table,)).fetchone()
        if n == 0:
          st.write(f"... creating {SNOWFLAKE_SCHEMA}.{table} by inferring schema from {file.name}")
          sql = f'''
create transient table {SNOWFLAKE_DATABASE}.{SNOWFLAKE_SCHEMA}.{table}
using template (
  select array_agg( object_construct(*) )
    from table(
      infer_schema(
        location => '@{SNOWFLAKE_DATABASE}.{SNOWFLAKE_META_SCHEMA}.{destination_blob_name}',
        file_format => '{SNOWFLAKE_DATABASE}.{SNOWFLAKE_META_SCHEMA}.{SNOWFLAKE_FILE_FORMAT}',
        ignore_case => true
      )
    )
  )
'''
          st.write(f"```{sql}```")
          c.execute(sql)

        st.write(f'... loading data into {SNOWFLAKE_DATABASE}.{SNOWFLAKE_SCHEMA}.{table}')
        sql = f'''
copy into {SNOWFLAKE_DATABASE}.{SNOWFLAKE_SCHEMA}.{table}
from @{SNOWFLAKE_DATABASE}.{SNOWFLAKE_META_SCHEMA}.{destination_blob_name}
     file_format = {SNOWFLAKE_DATABASE}.{SNOWFLAKE_META_SCHEMA}.{SNOWFLAKE_FILE_FORMAT}
     match_by_column_name = case_insensitive
'''
        st.write(f'```{sql}```')
        c.execute(sql)
        st.write('<iframe src="https://giphy.com/embed/XreQmk7ETCak0" width="480" height="360" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>', unsafe_allow_html=True)
        st.write(f'try running this in [Snowsight](https://{account}.snowflakecomputing.com):')
        st.write(f'''```
select *
  from {SNOWFLAKE_DATABASE}.{SNOWFLAKE_SCHEMA}.{table}
;
```''')

        blob.reload()
        blob.delete(if_generation_match=blob.generation)
