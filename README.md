CSVHub
======

Everyone loves Excel, and CSV files are **the** lingua franca of
data interchange, even after all these years.  Getting CSV-ed data
into Snowflake is fairly easy if you're savvy enough - set up a
file format, define a storage integration, create a stage, upload
the CSV data, build a table, and run a COPY operation.  Easy.

For the rest of the world, there's CSVHub.

This little streamlit app uses your Snowflake credentials to
connect to your instance – you pick the account, user, warehouse,
and role – and then provides a simple two-step process for getting
data out of CSV files and Excel into a table in Snowflake.

There is a little bit of Snowflake-side setup needed:

- The app works exclusively in the `CSVHUB` database.
- It uses a stage named `CSVHUB.PUBLIC.CSVHUB` for all loads.
- Tables are loaded in the `CSVHUB.UPLOADS` schema.
- It uses a file format named `CSVHUB.PUBLIC.CSVHUB`

Here's some SQL to get you started:

    -- create a place for csvhub to work
    create database if not exists csvhub;
    create schema if not exists csvhub.uploads;

    -- create a file format for CSV files
    create or replace file format csvhub.public.csv
      type = csv
      parse_header = true
      field_optionally_enclosed_by = '\"'
      skip_blank_lines = true
    ;

    -- create the stage that will house the CSV files
    create or replace stage csvhub.public.csvhub
      storage_integration = YOUR_STORAGE_INTEGRATION
      file_format = csvhub.uploads.csv
      url = 'gcs://YOUR-GCS-BUCKET-NAME/csvhub'
    ;

Note that you will need a storage integration to properly hook up
to your bucket.  Currently, CSVHub only supports GCS.  You can
find Snowflake documentation on setting up storage integrations
across all major cloud providers here:

  - [Amazon AWS](https://docs.snowflake.com/en/user-guide/data-load-s3-config-storage-integration)
  - [Google Cloud](https://docs.snowflake.com/en/user-guide/data-load-gcs-config)
  - [Microsoft Azure](https://docs.snowflake.com/en/user-guide/data-load-azure-config)

## Running CSVHub

The best way to run CSVHub is via its Docker image, which is
available from img.vivanti.com/pub/csvhub:

    $ docker run img.vivanti.com/pub/csvhub

However, you will need to set your configuration – at the very
least, CSVHub needs to know how to get to your GCS bucket!  All
configuration is done through environment variables.  Credentials
are supplied via a bind-mount.

These configuration (environment) variables are **required**:

  - `CSVHUB_GOOGLE_PROJECT` - The name of your Google Cloud
    Platform project, the one with all the numbers at the end.

  - `CSVHUB_GOOGLE_BUCKET` - The name of your Google Cloud Storage
    bucket (which must be globally unique)

You must also supply the service account JSON-formatted
credentials file at a fixed point _inside_ the running container,
at `/creds/google.json`.  This is best done via a
bind-mount; in Docker this is the `-v` flag.

By default, the Streamlit app will bind port 80 inside of the
container.  This is an EXPOSE'd port, in Docker parlance, so you
can either pass it through or explicitly remap a host port to it.
We recommend the latter, paired with a reverse proxy server like
`nginx` up front.

In Docker, port mapping is done with the `-P` (expose all known
ports as-is) or the `-p` (map specific ports) flags:

    $ docker run -p 7777:80 img.vivanti.com/pub/csvhub

The above command would make CSVHub available on the Docker host
on TCP port 7777.  Change this value to suit your configuration.

Putting all of this together, here's a rough template you can
modify as needed:

    $ docker run --restart=always \
        -p 7777:80 \
        -v ./google-sa-key.json:/creds/google.json:ro \
        -e CSVHUB_GOOGLE_PROJECT=<your-google-project> \
        -e CSVHUB_GOOGLE_BUCKET=<your-google-bucket>
        -e CSVHUB_DEFAULT_SNOWFLAKE_ACCOUNT=<your-snowflake-account> \
        img.vivanti.com/pub/csvhub

## Additional Configuration

There is more to CSVHub configuration than just the required
options.  Here are some optional configuration (environment)
variables you can set to influence its behavior:

  - `CSVHUB_GOOGLE_BLOB_PREFIX` - All files uploaded to the bucket
    will be prefixed with `csvhub/` – this is necessary for the
    proper operation of CSVHub internals.  However, you may put
    additional "directory" paths between that required prefix and
    the final name of your uploaded files.  If you are using the
    same GCS bucket for multiple CSVHub instances, for example,
    this can be useful to tell their uploaded files apart.

  - `CSVHUB_ALLOWED_SNOWFLAKE_ACCOUNTS` is a comma-delimited list
    of Snowflake account names in shortened form (i.e.
    `ab1234567.us-central.azure`) whose authenticated users are
    allowed to use this CSVHub instance.

    This acts as a security control on who is allowed to incur
    storage costs on the GCS bucket, since that access is not
    explicitly checked.

    By default, all Snowflake accounts are allowed.  You should
    probably change this.

    If you set the `CSVHUB_DEFAULT_SNOWFLAKE_ACCOUNT` to a value,
    only that account will be allowed to authenticate unless you
    also override `CSVHUB_ALLOWED_SNOWFLAKE_ACCOUNTS`

  - `CSVHUB_DEFAULT_SNOWFLAKE_ACCOUNT` contains the shortened name
    of the Snowflake account to authenticate to.  This is shown as
    a pre-filled value in the login sidebar.

  - `CSVHUB_DEFAULT_SNOWFLAKE_ROLE` contains the name of the role
     used by default during authentication.  This can be changed
     by the user, but shows up as a pre-filled value in the login
     form, as a convenience.

  - `CSVHUB_DEFAULT_SNOWFLAKE_WAREHOUSE` contains the name of the
    Snowflake warehouse to use for compute.  The user can change
    this; it shows up as a pre-filled value in the login form for
    convenience's sake.

  - `CSVHUB_SNOWFLAKE_DATABASE` contains the name of the database
    that houses the Snowflake side of CSVHub operations; the file
    format, stage, and destination schema.  Defaults to `csvhub`.

  - `CSVHUB_SNOWFLAKE_SCHEMA` contains the name of the schema to
    create tables in.  Defaults to `uploads`.

  - `CSVHUB_SNOWFLAKE_META_SCHEMA` identifies the schema where the
    machinery of CSVHub (stage, file format, etc.) lives.
    Defaults to `public`.

  - `CSVHUB_SNOWFLAKE_FILE_FORMAT` identifies the FILE FORMAT to
    use when parsing uploaded CSV files.  Defaults to `csv`.
