IMAGE ?= img.vivanti.com/pub/csvhub
TAG   ?= latest

build:
	docker build -t $(IMAGE):$(TAG) -f Dockerfile .

push: build
	docker push $(subst img,imgpush,$(IMAGE)):$(TAG)
