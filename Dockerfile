FROM ubuntu:22.04

EXPOSE 80
ENV STREAMLIT_SERVER_PORT 80

WORKDIR /streamlit
ENTRYPOINT ["streamlit", "run", "app.py"]

RUN apt-get update \
 && apt-get install -y python3 python3-pip
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
